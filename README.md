Manjaro Linux Archive Tools
========================

Introduction
------------
**manjaro-archivetools** is the project used to run the [Manjaro Linux Archive](). It's a turnkey solution to snapshot [Manjaro Linux](https://www.manjaro.org) packages repositories, ISOs images and boostrap tarballs. You can deploy one for your own needs.

Forked from the original [archivetools](https://github.com/seblu/archivetools/)

The **Archive** is built by rsync'ing [a Swedish Manjaro mirror](rsync://ftp.lysator.liu.se/pub/manjaro/), each day. *Rsync* features are used to transfer only the diff of new data from the previous snapshot and files are stored once with use of hardlinks.

Installation
------------
Create a pacman package and install it.

```
cd manjaro-archivetools
makepkg -sri
systemctl enable archive.timer
```

Debug
-----
```
cd manjaro-archivetools
export DEBUG=1
export ARCHIVE_CONFIG=archive.conf.test
./archive.sh
```

Note
------------
Current default configuration skips ISOs.

Dependencies
------------
- [Bash](http://www.gnu.org/software/bash/bash.html)
- [Rsync](http://rsync.samba.org/)
- [Hardlink](http://jak-linux.org/projects/hardlink/)
- [xz](http://tukaani.org/xz/)
- [util-linux](https://www.kernel.org/pub/linux/utils/util-linux/)


Sources
-------
**archivetools** sources are available on [github](https://github.com/seblu/archivetools/).

License
-------
**archivetools** and thus **manjaro-archive-tools** is licensed under the terms of [GPL v2](http://www.gnu.org/licenses/gpl-2.0.html).

Author
------
**archivetools** was started by *Sébastien Luttringer* in August 2013 to replace the former *Arch Linux Rollback Machine* service.

**manjaro-archive-tools** was forked by cscs